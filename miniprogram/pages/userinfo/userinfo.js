const app = getApp()

Page({
  data: {
    _id: "",
    radioShow : true,
    items: [
      { name: '业主', value: '业主' },
      { name: '小区物业管理', value: '小区物业管理' },
      { name: '周边店铺', value: '周边店铺' }
    ],
    array: [
      { name: '新洲·香樟美地', location:'汕头市金平区汕樟路128号'},
      { name: '悦泰春天', location: '汕樟路141号与党校路转角处' },
      { name: '金裕园', location: '广东省汕头市金平区北华路' },
      { name: '金丰社区', location: '广东省汕头市金平区金砂街道金丰路14号1座' }
    ]
  },
  onLoad: function () {
    var self = this;
    this.mapCtx = wx.createMapContext('myMap');
    wx.getLocation({
      type: 'gcj02',
      success(res) {
        self.setData({
          latitude: res.latitude,
          longitude: res.longitude,
          markers: [],
        });
      }
    }),
    this.queryUser();
  },

  radioChange: function (e) {
    this.setData({ radioShow: false });
  },

  toFinish: function(e) {
    //更新用户信息
    const db = wx.cloud.database({});
    const t_user = db.collection('user');
    t_user.doc(this.data._id).update({
      data: {
        type : 1
      }
    }).then(res => {
      // 更新数据成功
      console.log(res)
    }).catch(err => {
      // 更新数据失败
      console.log(err)
    })

     wx.navigateTo({
      url: '/pages/back/back'
    })
  },

  queryUser: function () {
    console.log("查询用户");
    const db = wx.cloud.database({});
    const t_user = db.collection('user');
    t_user.where({
      _openid: app.globalData.openid
    }).get({
      success: res => {
        console.log(res);
        var user = res.data[0];
        if (user != null) {
          this.setData({
            _id: user._id
          })
        }

      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
        console.error('[数据库] [查询记录] 失败：', err)
      }
    })
  }
})