// miniprogram/pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    num: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  toSure: function() {
    wx.navigateTo({
      url: '/pages/success/success'
    })
  },
  
  /* 加数 */
  addCount: function (e) {
    console.log("刚刚您点击了加1");
    var num = this.data.num;
    // 总数量-1  
    if (num < 2) {
      this.data.num++;
    }
    // 将数值与状态写回  
    this.setData({
      num: this.data.num
    });
  },

  /* 减数 */
  delCount: function (e) {
    console.log("刚刚您点击了减1");
    var num = this.data.num;
    // 商品总数量-1
    if (num > 1) {
      this.data.num--;
    }
    
    // 将数值与状态写回  
    this.setData({
      num: this.data.num
    })
  },

  getCount: function (e) {
    var num = this.data.num;
    console.log(num);
    wx.showToast({
      title: "数量：" + num + "",
    })
  }
})