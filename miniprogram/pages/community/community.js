// miniprogram/pages/community/community.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [],
    notLogin: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {
    this.queryUser();
  },

  toSearch: function(){
    const db = wx.cloud.database({});
    const t_message = db.collection('message');
    t_message.where({
      _openid: app.globalData.openid,
      type: 1
    }).get({
      success: res => {
        console.log(res.data);
        this.setData({
          array: res.data
        })
      }
    })
  },

  toDetail: function() {
    wx.navigateTo({
      url: '/pages/detail/detail'
    })
  },

  queryUser: function () {
    const db = wx.cloud.database({});
    const t_user = db.collection('user');
    t_user.where({
      _openid: app.globalData.openid
    }).get({
      success: res => {
        var user = res.data[0];
        if (user != null) {
          this.setData({
            notLogin: false
          })
        }
        //获取列表数据

        const t_message = db.collection('message');
        t_message.where({
          _openid: app.globalData.openid
        }).get({
          success: res => {
            console.log(res.data);
            this.setData({
              array: res.data
            })
          }
        })

      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
        console.error('[数据库] [查询记录] 失败：', err)
      }
    })
  }
})